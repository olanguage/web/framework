# O Language Web Framework (Not completed yet)

It is a web framework example project prepared with O Language. She's currently in testing. 

In order to run and edit sample codes in the system, you must take the project to your system with [opkg](https://olproject.github.io/opkg/).
```
$ opkg craft github.com/olproject/olf projectname
```

If you do not need it, you can delete the docs folder;
```
$ rm -rf projectname/docs
```

You can then start the process using [ops](https://olproject.github.io/ops/).
```
$ ops start
```

To stop processes;
```
$ ops stop
```