FROM olproject/olang

RUN mkdir /olf
COPY . /olf/
WORKDIR /olf

CMD ["olang","boot.ola"]
EXPOSE 10000
